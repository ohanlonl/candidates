# Coding Challenge
_Author: Liam O'Hanlon_

## Features
+ Changed inline JS to external file.
+ Swapped from using CSS to Stylus which is compiled to CSS by a file watcher.
+ Filter by candidate skills function implemented.
+ New search by name function implemented.
+ Aesthetic overhaul using Bootstrap 4 and FontAwesome 5.
+ Responsive design using media queries targeting following device widths as breakpoints:
    + Small-Medium Mobiles: 320px-375px,
    + Large Mobile: 425px,
    + Tablet: 768px,
    + Split screen: 900-960px,
    + Laptops: 1024px & 1280px & 1366,
    + Desktop: 1440+px.
+ Implemented limit forcing page to only keep one set of results at a time instead of appending new results.

## Screenshots
As seen at 1920x1080px. Displayed below is the demo table I kept in as a demonstration of example output. This table is only seen once when the page is loaded, once a search is performed the demo table will disappear.<br>
<img src="imgs/desktopDemo.JPG">
<br><br>
In the below image, the page can be seen using the media query designed for split screen. Here we can see the filter by candidate skills functionality being demonstrated.<br>
<img src="imgs/splitLanguage.JPG">
<br><br>
In this final screenshot, we can see the newly implemented search by name functionality being demonstrated as it would appear on a mobile device.<br>
<img src="imgs/nameSearchMobile.png">
