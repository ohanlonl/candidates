const newCandidates = [
	{ name: "Kerrie", skills: ["JavaScript", "Docker", "Ruby"] },
	{ name: "Mario", skills: ["Python", "AWS"] },
	{ name: "Jacquline", skills: ["JavaScript", "Azure"] },
	{ name: "Kathy", skills: ["JavaScript", "Java"] },
	{ name: "Anna", skills: ["JavaScript", "AWS"] },
	{ name: "Matt", skills: ["PHP", "AWS"] },
	{ name: "Matt", skills: ["PHP", ".NET", "Docker"] },
];

function removeRowsFromTable(table) {
	const rows = table.getElementsByTagName("tr");
	while (rows.length > 1) {
		table.deleteRow(1);
	}
}

function insertCandidate(tbody, name, skills) {
	const newRow = tbody.insertRow();
	const nameCell = newRow.insertCell();
	const skillCell = newRow.insertCell();
	const candidateName = document.createTextNode(name);
	const candidateSkills = document.createTextNode(skills.join(', '));
	nameCell.appendChild(candidateName);
	skillCell.appendChild(candidateSkills);
}

function addCandidatesToTable(table, candidates) {
	candidates.forEach(candidate => insertCandidate(table, candidate.name, candidate.skills));
}

// Enforce limit on number of results that can show on the page at any given time
function enforceLimit(limit) {
	var noOfElements = document.querySelectorAll("#candidates_table").length;
	if(noOfElements >= limit) {
		var oldElement = document.getElementById("container");
		if(oldElement != null) {
			oldElement.parentNode.removeChild(oldElement);
		} else {
			var altElement = document.getElementById("candidates_table");
			altElement.parentNode.removeChild(altElement);
		}
	}
}

// Outputs candidates who have the skill passed as the parameter
function filterCandidateBySkill(skill) {
	// Declare array that will contain candidates who met criteria
	var filteredCandidates = [];
	const candidatesTable = document.getElementById("candidates_table");
	const newCandidatesTable = candidatesTable.cloneNode(true);
	// Set limit to 1 set of results
	enforceLimit(1);
	// Iterate through candidates
	for(var x=0; x<newCandidates.length; x++) {
		// Iterate through all candidates skills
		var tempSkills = newCandidates[x].skills;
		for(var y=0; y<tempSkills.length; y++) {
			// If the candidate has the skill we are looking for, push to array
			if(tempSkills[y] == skill){filteredCandidates.push(newCandidates[x]);}
		}
	}
	// Output results
	removeRowsFromTable(newCandidatesTable);
	const newTbody = newCandidatesTable.getElementsByTagName('tbody')[0];
	addCandidatesToTable(newTbody, filteredCandidates);
	document.body.appendChild(newCandidatesTable);
}

// Returns percentage denoting similarity between strings
function compareStrings(string, string2) {
	for (var i = 0, len = Math.max(string.length, string2.length); i < len; i++)
		if (string.charAt(i) != string2.charAt(i))
			return Math.round(i / len * 100);
}

// Perform name search and print candidates who meet criteria
function search() {
	var nameToSearch = document.getElementById('search-input').value;
	var filteredCandidates = [];
	const candidatesTable = document.getElementById("candidates_table");
	const newCandidatesTable = candidatesTable.cloneNode(true);
	enforceLimit(1);
	// Iterate through candidates
	for(var x=0; x<newCandidates.length; x++) {
		var tempName = newCandidates[x].name;
		// Compare strings and push results to filteredCandidates if strings are more than 20% similar
		if(compareStrings(tempName,nameToSearch) > 20){ filteredCandidates.push(newCandidates[x]); } else {
			// Change user input to appropriate format and attempt comparison again
			nameToSearch = nameToSearch.charAt(0).toUpperCase() + nameToSearch.substr(1);
			if(compareStrings(tempName,nameToSearch) > 20){ filteredCandidates.push(newCandidates[x]); }
		}
	}
	// Output results
	removeRowsFromTable(newCandidatesTable);
	const newTbody = newCandidatesTable.getElementsByTagName('tbody')[0];
	addCandidatesToTable(newTbody, filteredCandidates);
	document.body.appendChild(newCandidatesTable);
}

